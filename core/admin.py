from django.contrib import admin

from .models import Class, School, Student, Parent, SchoolType, TeachingStaff, SupportStaff, Subject, Mark, Exam, User

admin.site.register(User)


@admin.register(School)
class AdminSchool(admin.ModelAdmin):
    list_display = ('name', 'type')


@admin.register(Student)
class AdminStudent(admin.ModelAdmin):
    list_display = ('user', 'regno', 'school')


@admin.register(Class)
class AdminClass(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(Parent)
admin.site.register(SchoolType)
admin.site.register(TeachingStaff)
admin.site.register(SupportStaff)
admin.site.register(Mark)
admin.site.register(Subject)
admin.site.register(Exam)

admin.site.site_title = 'CHINSKI'
admin.site.index_title = 'CHINSKI administration'
admin.site.site_header = 'CHINSKI admin'
