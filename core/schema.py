import graphene
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from chinski.settings import MEDIA_URL
from .models import Student, User, School, Parent, TeachingStaff, Class, SchoolType


class StudentNode(DjangoObjectType):
    class Meta:
        model = Student
        interfaces = (relay.Node,)
        filter_fields = ()


class UserNode(DjangoObjectType):
    class Meta:
        model = User

        filter_fields = ()
        interfaces = (relay.Node,)


class SchoolNode(DjangoObjectType):
    class Meta:
        model = School
        interfaces = (relay.Node,)
        filter_fields = ()

    def resolve_logo(self, *_):
        return '{}{}'.format(MEDIA_URL, self.logo)


class ParentNode(DjangoObjectType):
    class Meta:
        model = Parent
        filter_fields = ()
        interfaces = (relay.Node,)

    count = graphene.String()


class TeacherNode(DjangoObjectType):
    class Meta:
        model = TeachingStaff
        interfaces = (relay.Node,)
        filter_fields = ()


class ClassNode(DjangoObjectType):
    class Meta:
        model = Class
        interfaces = (relay.Node,)
        filter_fields = ()


class SchoolTypeNode(DjangoObjectType):
    class Meta:
        model = SchoolType

        interfaces = (relay.Node,)
        filter_fields = ()


class Query(graphene.ObjectType):
    students = DjangoFilterConnectionField(StudentNode)

    school = relay.Node.Field(SchoolNode)

    schools = DjangoFilterConnectionField(SchoolNode)



schema = graphene.Schema(query=Query)
