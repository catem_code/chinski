from django.contrib.auth.models import AbstractUser
from django.db import models


def upload_student_photo(instance, filename):
    return 'student_{0}/{1}'.format(instance.regno, filename)


def upload_school_logo(instance, filename):
    return 'logos/school_{0}/{1}'.format(instance.name, filename)


class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    dob = models.DateField()

    regno = models.CharField(max_length=20, unique=True)

    photo = models.ImageField(upload_to=upload_student_photo)
    finger = models.CharField(max_length=100, blank=True)

    parent = models.ForeignKey('Parent', on_delete=models.CASCADE, related_name='students')
    school = models.ForeignKey('School', on_delete=models.CASCADE, related_name='students')
    student_class = models.ForeignKey('Class', on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    @property
    def get_full_name(self):
        return '%s %s %s' % (self.user.first_name, self.user.last_name, self.user.username)

    def get_age(self):
        pass

    def __repr__(self):
        return self.get_full_name

    def __str__(self):
        return self.get_full_name


class Parent(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=15)

    school = models.ForeignKey('School', on_delete=models.CASCADE)

    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __repr__(self):
        return self.user.username

    def __str__(self):
        return self.user.username


class School(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField()
    motto = models.TextField()

    type = models.ForeignKey('SchoolType', on_delete=models.CASCADE)
    logo = models.ImageField(upload_to=upload_school_logo)
    location = models.CharField(max_length=200)
    exact_location = models.CharField(max_length=200)
    school_head = models.ForeignKey(User, on_delete=models.CASCADE)

    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class SchoolType(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField()

    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


class TeachingStaff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=13)
    school = models.ForeignKey('School', on_delete=models.CASCADE, related_name='teachers')
    subjects = models.ManyToManyField('Subject', related_name='teachers')

    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __repr__(self):
        return self.user.username

    def __str__(self):
        return self.user.username


class SupportStaff(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=13)
    school = models.ForeignKey('School', on_delete=models.CASCADE, related_name='supportstaff')

    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __repr__(self):
        return self.user.username

    def __str__(self):
        return self.user.username


class Class(models.Model):
    name = models.CharField(max_length=100)
    level = models.CharField(max_length=200)

    class_supervisor = models.ForeignKey(TeachingStaff, on_delete=models.CASCADE)
    subjects = models.ManyToManyField('Subject', related_name='classes')

    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


class Subject(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField(blank=True)
    sub_code = models.CharField(max_length=255, unique=True, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


class Mark(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    class_level = models.ForeignKey(Class, on_delete=models.CASCADE)
    marks = models.DecimalField(max_digits=2, max_length=4, decimal_places=2)
    exam = models.ForeignKey('Exam', related_name='marks', null=True, blank=True, on_delete=models.CASCADE)

    def __repr__(self):
        return self.subject.name + '->' + self.marks

    def __str__(self):
        return self.subject.name + '->' + self.marks


class Exam(models.Model):
    """ Store Exam Information """
    name = models.CharField(max_length=200, null=False)
    exam_type = models.CharField(max_length=150, null=True, blank=True)
    exam_date = models.DateField(null=True, blank=True)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name
